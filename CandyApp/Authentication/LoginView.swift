//
//  LoginView.swift
//  CandyApp
//
//  Created by Александр Ощепков on 09.09.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

import UIKit

class LoginView: AuthBaseView {
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    var isPasswordHidden: Bool!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupPasswordField()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Actions
    
    func setupPasswordField() {
        isPasswordHidden = true
        
        let paddingRightView = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: passwordTextField.frame.height))
        passwordTextField.rightView = paddingRightView
        passwordTextField.rightViewMode = .always
    }
    
    // MARK: IBActions
    
    @IBAction func togglePasswordAction(_ sender: UIButton) {
        let passwordField = textFields.last
        
        if(isPasswordHidden == true) {
            passwordField?.isSecureTextEntry = false
            isPasswordHidden = false
            sender.alpha = 1;
        } else {
            passwordField?.isSecureTextEntry = true
            isPasswordHidden = true
            sender.alpha = 0.5;
        }
    }
    
    @IBAction func loginAction(_ sender: UIButton) {
        let username = usernameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let password = passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if username?.characters.count == 0 || password?.characters.count == 0 {
            showAlert(title: "Warning", message: "All fields must be fill in.")
        } else {
            DataManager.shared.getUserByUsernameAndPassword(username: username!, password: password!, completionHandler: { results in
                if results.count > 0 {
                    authComplete(username: username!)
                } else {
                    showAlert(title: "Error", message: "Incorrect username or password.")
                }
            })
        }
    }
}
