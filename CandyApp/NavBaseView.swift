//
//  NavBaseView.swift
//  CandyApp
//
//  Created by Александр Ощепков on 07.10.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

import UIKit
import SideMenu

class NavBaseView: UIViewController {
    
    @IBOutlet weak var titleSection: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet var switchControls: [UISwitch]!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepareBeforeShow()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Actions
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func keyboardWillShow(notification:NSNotification){
        //give room at the bottom of the scroll view, so it doesn't cover up anything the user needs to tap
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        scrollView.contentInset = contentInset
    }
    
    func keyboardWillHide(notification:NSNotification){
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }
    
    func prepareBeforeShow() {
        // Remove bottom border for nav bar
        self.navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor(hexString: Consts.Typo.PinkDarker)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.backIndicatorImage = UIImage()
        
        // Add bottom shadow for nav bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor(hexString: Consts.Typo.PinkDark)?.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 1.0
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 38
        
        // Set background color for title section
        titleSection.backgroundColor = UIColor(hexString: Consts.Typo.PinkMain)
        
        // Set-up for left side menu
        SideMenuManager.menuPresentMode = .menuSlideIn
        SideMenuManager.menuWidth = view.frame.width - 75
        SideMenuManager.menuAnimationBackgroundColor = UIColor.clear
        SideMenuManager.menuShadowColor = UIColor(hexString: Consts.Typo.PinkDarker)!
        SideMenuManager.menuShadowRadius = 24
        
        // Set-up for switches
        if switchControls != nil {
            for switchControl in switchControls {
                switchControl.tintColor = UIColor(hexString: Consts.Typo.GreenLight)
                switchControl.onTintColor = UIColor(hexString: Consts.Typo.GreenLight)
            }
        }
    }
    
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
