//
//  Consts.swift
//  CandyApp
//
//  Created by Александр Ощепков on 09.09.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

struct Consts {
    struct Typo {
        
        // Colors
        static let PinkMain = "#FFEEEE"
        static let PinkDarker = "#FFE6E6"
        static let PinkDark = "#F3D0D0"
        static let BlueMain = "#243B6B"
        static let GreenLight = "#93FFDF"
        
        // Fonts
        static let NavigationFont = "Avenir-Black"
        
        //Keys
        static let UsernameKey = "usernameKey"
    }
}
