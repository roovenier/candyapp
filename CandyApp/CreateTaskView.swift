//
//  CreateTaskView.swift
//  CandyApp
//
//  Created by Александр Ощепков on 07.10.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

import UIKit
import RMActionController
import RMDateSelectionViewController
import DropDown

class CreateTaskView: NavBaseView, UITextViewDelegate, UITextFieldDelegate {

    @IBOutlet weak var taskTitleField: UITextView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet var textFields: [UITextField]!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var priorityTextField: UITextField!
    @IBOutlet weak var alarmSwitch: UISwitch!
    @IBOutlet weak var notificationSwitch: UISwitch!
    
    let titlePlaceholder: String = "Write here..."
    var selectedDate: Date = Date()
    var selectedPriority: Int = 0
    var dropDown: DropDown?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        
        view.addGestureRecognizer(tap)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Actions
    
    func setupView() {
        
        // Set save button
        saveButton.backgroundColor = UIColor(hexString: Consts.Typo.BlueMain)
        saveButton.layer.cornerRadius = saveButton.frame.height/2
        saveButton.layer.shadowColor = UIColor(hexString: Consts.Typo.BlueMain)?.cgColor
        saveButton.layer.shadowOffset = CGSize(width: 0, height: 5)
        saveButton.layer.shadowRadius = 6
        saveButton.layer.shadowOpacity = 0.3
        
        view.addSubview(saveButton)
        
        saveButton.setImage(UIImage(named: "check"), for: UIControlState.normal)
        
        self.view.layoutIfNeeded()
        
        // Set styles for text fields
        if let _ = textFields?.count {
            for textField in textFields {
                textField.layer.cornerRadius = 8.0
                textField.layer.borderWidth = 1.0
                textField.layer.borderColor = UIColor(hexString: Consts.Typo.BlueMain, alpha: 0.2)?.cgColor
                
                let paddingLeftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: textField.frame.height))
                textField.leftView = paddingLeftView
                textField.leftViewMode = .always
                
                let paddingRightView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: textField.frame.height))
                textField.rightView = paddingRightView
                textField.rightViewMode = .always
                
                textField.delegate = self
            }
        }
        
        dateTextField.textColor = UIColor(hexString: Consts.Typo.BlueMain, alpha: 0.5)
        
        // Set priority dropdown
        dropDown = DropDown()
        dropDown?.anchorView = priorityTextField
        dropDown?.dataSource = DataManager.shared.getPriorities()
        dropDown?.direction = .bottom
        dropDown?.bottomOffset = CGPoint(x: 0, y:(dropDown?.anchorView?.plainView.bounds.height)!)
        dropDown?.selectionAction = { (index, name) in
            self.selectedPriority = index
            self.priorityTextField.text = name
        }
        //SelectionClos
    }
    
    func removeFocusFromField() {
        taskTitleField.resignFirstResponder()
    }
    
    func dateToString(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short
        return dateFormatter.string(from: date)
    }
    
    func setDateFieldText(date: Date) {
        dateTextField.textColor = UIColor(hexString: Consts.Typo.BlueMain, alpha: 1.0)
        dateTextField.text = dateToString(date: date)
    }
    
    // MARK: IBActions
    
    @IBAction func closeViewAction(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveTaskAction(_ sender: UIButton) {
        let taskTitle = taskTitleField.text.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if taskTitle.characters.count != 0 && taskTitle != titlePlaceholder {
            if UserDefaults.standard.object(forKey: Consts.Typo.UsernameKey) != nil {
                DataManager.shared.getUserByUsername(username: UserDefaults.standard.object(forKey: Consts.Typo.UsernameKey) as! String) { (users: [User]) in
                    DataManager.shared.saveNewTask(completedDate: selectedDate, priority: selectedPriority, title: taskTitle, user: users.first!) { task in
                        print("task \(task)")
                    }
                }
            }
        } else {
            showAlert(title: "Warning", message: "You must fill in the title of a new task")
        }
    }
    
    // MARK: UITextViewDelegate
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.alpha == 0.5 {
            textView.text = nil
            textView.alpha = 1.0
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = titlePlaceholder
            textView.alpha = 0.5
        }
    }
    
    // MARK: UITextFieldDelegate
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == dateTextField {
            let selectAction = RMAction<UIDatePicker>(title: "Select", style: RMActionStyle.done) { controller in
                self.selectedDate = controller.contentView.date
                self.setDateFieldText(date: controller.contentView.date)
            }
            
            let cancelAction = RMAction<UIDatePicker>(title: "Cancel", style: RMActionStyle.cancel, andHandler: nil)
            
            let dateSelectionController = RMDateSelectionViewController(style: .white, title: "Completed date", message: "Please select the end date for this task.", select: selectAction, andCancel: cancelAction)!;
            
            present(dateSelectionController, animated: true, completion: nil)
        } else if textField == priorityTextField {
            dropDown?.show()
        }
        
        return false
    }
}
