//
//  DataManager.swift
//  CandyApp
//
//  Created by Александр Ощепков on 24.09.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

import UIKit
import CoreData
import CryptoSwift

class DataManager: NSObject {
    private override init() {}
    
    static let shared = DataManager()
    
    let aesKey = "D77E0AFD4F82AB8EE07407C9C279FB1B"
    let aesIv = "9274EC89918AA4CC"
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "CandyApp")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    // MARK: Request Actions
    
    func getUserByUsername(username: String, completionHandler: (_ user: [User]) -> Void) {
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "User")
        fetchRequest.predicate = NSPredicate(format: "username = %@", username)
        
        do {
            let results = try self.persistentContainer.viewContext.fetch(fetchRequest) as! [User]
            completionHandler(results)
        } catch {
            print("Error fetch user \(error)")
            completionHandler([])
        }
    }
    
    func getUserByUsernameAndPassword(username: String, password: String, completionHandler: (_ user: [User]) -> Void) {
        let passwordEncryprted = encryptPassword(password: password) as NSData
        
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "User")
        fetchRequest.predicate = NSPredicate(format: "username = %@ AND password = %@", argumentArray: [username, passwordEncryprted])
        
        do {
            let results = try self.persistentContainer.viewContext.fetch(fetchRequest) as! [User]
            completionHandler(results)
        } catch {
            print("Error fetch user \(error)")
            completionHandler([])
        }
    }
    
    func saveNewUser(username: String, password: String, completionHandler: (_ user: User) -> Void) {
        let passwordEncryprted = encryptPassword(password: password)
        
        let user = NSEntityDescription.insertNewObject(forEntityName: "User", into: self.persistentContainer.viewContext) as! User
        user.username = username
        user.password = passwordEncryprted as NSData
        do {
            try self.persistentContainer.viewContext.save()
            completionHandler(user)
        } catch {
            fatalError("Failure to save context: \(error)")
        }
    }
    
    func getPriorities() -> Array<String> {
        var prioritiesArray = [String]()
        
        if let path = Bundle.main.path(forResource: "Data", ofType: "plist") {
            if let dict = NSDictionary(contentsOfFile: path) {
                prioritiesArray = (dict.value(forKey: "Priorities") as? Array<String>)!
            }
        }
        return prioritiesArray
    }
    
    func saveNewTask(completedDate: Date, priority: Int, title: String, user: User, completionHandler: (_ task: Task) -> Void) {
        let task = NSEntityDescription.insertNewObject(forEntityName: "Task", into: self.persistentContainer.viewContext) as! Task
        task.completedDate = completedDate as NSDate
        task.isComplete = false
        task.priority = Int16(priority)
        task.title = title
        task.user = user
        
        do {
            try self.persistentContainer.viewContext.save()
            completionHandler(task)
        } catch {
            fatalError("Failure to save context: \(error)")
        }
    }
    
    func getTasksForToday(user: User, completionHandler: ([Task]) -> Void) {
        var calendar = Calendar.current
        calendar.timeZone = NSTimeZone.local
        
        let dateFrom = calendar.startOfDay(for: Date())
        var components = calendar.dateComponents([.year, .month, .day, .hour, .minute],from: dateFrom)
        components.day! += 1
        let dateTo = calendar.date(from: components)!
        
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Task")
        let datePredicate = NSPredicate(format: "((%@ <= completedDate) AND (completedDate < %@)) AND user = %@", argumentArray: [dateFrom, dateTo, user])
        //let datePredicate = NSPredicate(format: "user = %@", argumentArray: [user])
        fetchRequest.predicate = datePredicate
        
        do {
            let results = try self.persistentContainer.viewContext.fetch(fetchRequest) as! [Task]
            completionHandler(results)
        } catch {
            print("Error fetch user \(error)")
            completionHandler([])
        }
    }
    
    func getTasksForNextDays(user: User, completionHandler: ([Task]) -> Void) {
        var calendar = Calendar.current
        calendar.timeZone = NSTimeZone.local
        
        let dateFrom = calendar.startOfDay(for: Date())
        var components = calendar.dateComponents([.year, .month, .day, .hour, .minute],from: dateFrom)
        components.day! += 1
        let dateTo = calendar.date(from: components)!
        
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Task")
        let datePredicate = NSPredicate(format: "completedDate > %@ AND user = %@", argumentArray: [dateTo, user])
        fetchRequest.predicate = datePredicate
        
        do {
            let results = try self.persistentContainer.viewContext.fetch(fetchRequest) as! [Task]
            completionHandler(results)
        } catch {
            print("Error fetch user \(error)")
            completionHandler([])
        }
    }
    
    // MARK: Helper Actions
    
    func encryptPassword(password: String) -> Data {
        let data = password.data(using: .utf8)!
        let encrypted = try! AES(key: aesKey, iv: aesIv, blockMode: .CBC, padding: .pkcs7).encrypt([UInt8](data))
        return Data(encrypted)
    }
    
    // DECRYPT
    // let decrypted = try! AES(key: key, iv: iv, blockMode: .CBC, padding: .pkcs7).decrypt([UInt8](passwordEncrypted))
    // let decryptedData = Data(decrypted)
    // print(String(data: decryptedData, encoding: String.Encoding.utf8) as String!)
}
