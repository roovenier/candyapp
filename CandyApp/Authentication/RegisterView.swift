//
//  RegisterView.swift
//  CandyApp
//
//  Created by Александр Ощепков on 11.09.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

import UIKit
import CoreData
import CryptoSwift

class RegisterView: AuthBaseView {
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var passwordConfirmTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: IBActions
    
    @IBAction func loginAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func registerSubmit(_ sender: UIButton) {
        let username = usernameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let password = passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let passwordConfirm = passwordConfirmTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if (username?.characters.count)! == 0 || (password?.characters.count)! == 0 || (passwordConfirm?.characters.count)! == 0 {
            showAlert(title: "Warning", message: "All fields must be fill in.")
        } else if password! != passwordConfirm {
            showAlert(title: "Warning", message: "Passwords don't match.")
        } else {
            DataManager.shared.getUserByUsername(username: username!, completionHandler: { results in
                if results.count > 0 {
                    showAlert(title: "Warning", message: "This name is already taken.")
                } else {
                    DataManager.shared.saveNewUser(username: username!, password: password!, completionHandler: { user in
                        authComplete(username: username!)
                    })
                }
            });
        }
    }
}
