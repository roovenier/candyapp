//
//  AuthBaseView.swift
//  CandyApp
//
//  Created by Александр Ощепков on 11.09.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

import UIKit
import SwiftHEXColors

class AuthBaseView: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var textFields: [UITextField]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        
        view.addGestureRecognizer(tap)

        prepareViewBeforeShow()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let index = textFields.index(of: textField)!
        
        if index < textFields.count - 1 {
            textFields[index + 1].becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        
        return false
    }

    // MARK: Actions
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func keyboardWillShow(notification:NSNotification){
        //give room at the bottom of the scroll view, so it doesn't cover up anything the user needs to tap
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        scrollView.contentInset = contentInset
    }
    
    func keyboardWillHide(notification:NSNotification){
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }
    
    func prepareViewBeforeShow() {
        view.backgroundColor = UIColor(hexString: Consts.Typo.PinkMain)
        
        if let _ = textFields?.count {
            for textField in textFields {
                textField.layer.cornerRadius = 8.0
                textField.layer.borderWidth = 1.0
                textField.layer.borderColor = UIColor(hexString: Consts.Typo.PinkDarker)?.cgColor
                
                let paddingLeftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: textField.frame.height))
                textField.leftView = paddingLeftView
                textField.leftViewMode = .always
                
                let paddingRightView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: textField.frame.height))
                textField.rightView = paddingRightView
                textField.rightViewMode = .always
            }
        }
    }
    
    // MARK: Actions
    
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func authComplete(username: String) {
        UserDefaults.standard.set(username, forKey: Consts.Typo.UsernameKey)
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "IndexController")
        vc?.modalPresentationStyle = .fullScreen
        self.present(vc!, animated: true, completion: nil)
    }
}
