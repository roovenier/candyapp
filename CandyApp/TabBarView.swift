//
//  TabBarView.swift
//  CandyApp
//
//  Created by Александр Ощепков on 07.10.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

import UIKit

extension UIImage {
    func createSelectionIndicator(color: UIColor, size: CGSize, lineWidth: CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(CGRect(origin: CGPoint(x: 0,y :size.height - lineWidth), size: CGSize(width: size.width, height: lineWidth)))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}

class TabBarView: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        tabBar.isTranslucent = false
        tabBar.barTintColor = UIColor(hexString: Consts.Typo.PinkDarker)
        
        UITabBar.appearance().shadowImage = UIImage()
        UITabBar.appearance().backgroundImage = UIImage()
        
        if  let arrayOfTabBarItems = self.tabBar.items as AnyObject as? NSArray, let tabBarItem = arrayOfTabBarItems[2] as? UITabBarItem {
            tabBarItem.isEnabled = false
            tabBarItem.selectedImage = UIImage()
        }
        
        setupAddingButton()
    }
    
    override func viewWillLayoutSubviews() {
        var tabFrame = self.tabBar.frame
        // - 40 is editable , the default value is 49 px, below lowers the tabbar and above increases the tab bar size
        tabFrame.size.height = 56
        tabFrame.origin.y = self.view.frame.size.height - 56
        self.tabBar.frame = tabFrame
        
        tabBar.selectionIndicatorImage = UIImage().createSelectionIndicator(color: UIColor(hexString: Consts.Typo.BlueMain)!, size: CGSize(width: tabBar.frame.width/CGFloat(tabBar.items!.count), height: tabBar.frame.height), lineWidth: 4.0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Actions
    
    func setupAddingButton() {
        let addButton = UIButton(frame: CGRect(x: 0, y: 0, width: 56, height: 56))
        var addButtonFrame = addButton.frame
        addButtonFrame.origin.y = (self.view.bounds.height - addButtonFrame.height) - 16
        addButtonFrame.origin.x = self.view.bounds.width / 2 - addButtonFrame.size.width / 2
        addButton.frame = addButtonFrame
        
        addButton.backgroundColor = UIColor(hexString: Consts.Typo.BlueMain)
        addButton.layer.cornerRadius = addButtonFrame.height/2
        addButton.layer.shadowColor = UIColor(hexString: Consts.Typo.BlueMain)?.cgColor
        addButton.layer.shadowOffset = CGSize(width: 0, height: 5)
        addButton.layer.shadowRadius = 6
        addButton.layer.shadowOpacity = 0.3
        
        view.addSubview(addButton)
        
        addButton.setImage(UIImage(named: "plus"), for: UIControlState.normal)
        addButton.addTarget(self, action: #selector(addButtonAction(sender:)), for: UIControlEvents.touchUpInside)
        
        self.view.layoutIfNeeded()
    }
    
    // Menu Button Touch Action
    func addButtonAction(sender: UIButton) {
        //self.selectedIndex = 2
        // console print to verify the button works
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "CreateTaskController")
        //vc?.modalPresentationStyle = .fullScreen
        let navEditorViewController: UINavigationController = UINavigationController(rootViewController: vc!)
        present(navEditorViewController, animated: true, completion: nil)
    }
}
