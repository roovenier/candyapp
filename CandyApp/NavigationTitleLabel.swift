//
//  NavigationTitleLabel.swift
//  CandyApp
//
//  Created by Александр Ощепков on 09.09.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

import UIKit

open class NavigationTitleLabel: UILabel {
    @IBInspectable open var characterSpacing:CGFloat = 1 {
        didSet {
            let attributedString = NSMutableAttributedString(string: self.text!)
            attributedString.addAttribute(NSKernAttributeName, value: self.characterSpacing, range: NSRange(location: 0, length: attributedString.length))
            self.attributedText = attributedString
        }
        
    }
}
