//
//  TodoView.swift
//  CandyApp
//
//  Created by Александр Ощепков on 07.10.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

import UIKit

class TodoView: NavBaseView {

    override func viewDidLoad() {
        super.viewDidLoad()

        let username = UserDefaults.standard.object(forKey: Consts.Typo.UsernameKey)
        
        DataManager.shared.getUserByUsername(username: username as! String) { (users: [User]) in
            print(users.first!)
            DataManager.shared.getTasksForToday(user: users.first!, completionHandler: { (tasks: [Task]) in
                print("Today \(tasks.count)")
            })
            
            DataManager.shared.getTasksForNextDays(user: users.first!, completionHandler: { (tasks: [Task]) in
                print("Next days \(tasks.count)")
            })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
